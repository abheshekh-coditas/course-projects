const randomNumber = function(min, max){
    return Math.floor(Math.random() *(max-min)) + min;
}

const app = Vue.createApp({
    data(){
        return {
            playerHealth: 100,
            monsterHealth: 100,
            currRounds: 0,
            winner: NaN, 
            logs: []
        };
    },
    watch: {
        playerHealth(value) {
            if(value <=0 && this.monsterHealth <= 0) {
                this.winner = 'tie';
            }
            else if(value <=0){
                this.winner = 'm';
            }
        },
        monsterHealth(value) {
            if(value <=0 && this.playerHealth <= 0) {
                this.winner = 'tie';
            }
            else if(value <=0){
                this.winner = 'p';
            }
        }
    },
    computed: {
        monsterHealthC() {
            return{
                width: this.monsterHealth + '%'
            };
        },
        playerHealthC() {
            return{
                width: this.playerHealth + '%'
            };
        }
    },
    methods: {
        restart() {
            this.playerHealth = 100;
            this.monsterHealth = 100;
            this.currRounds = 0;
            this.winner = NaN; 
            this.logs= [];
        },
        attackMonster() {
            this.currRounds += 1;
            attackValue = randomNumber(5, 10);
            this.monsterHealth -= attackValue;
            this.logMsgs('player','attack', attackValue);
            this.attackPlayer();
        },
        specialAttackMonster() {
            this.currRounds = 0;
            attackValue = randomNumber(8, 15);
            this.monsterHealth -= attackValue;
            this.logMsgs('player','attack', attackValue);
            this.attackPlayer();
        },
        attackPlayer() {
            attackValue = randomNumber(10, 15);
            this.playerHealth -= attackValue;
            this.logMsgs('monster','attack', attackValue);
        },
        heal() {
            this.currRounds = 0;
            healValue = randomNumber(8, 15);
            if(this.playerHealth + healValue >= 100){
                healValue = 100 - this.playerHealth;
                this.playerHealth = 100;
                this.logMsgs('player','heal', healValue);
            }else{
                this.playerHealth += healValue;
                this.logMsgs('player','Heal', healValue);
            }
            this.attackPlayer();
        },
        surrender() {
            this.winner = 'm';
        },
        logMsgs(who, what, value) {
            this.logs.unshift({
                actionBy: who,
                actionType: what,
                actionValue: value,
            });
        }
    }
});

app.mount('#game');  
console.log("in JS");
class typing {
  constructor() {
    // this.txtElemet = document.getElementById("id1");
    this.spanElemet = document.getElementById("id2");
    this.colors = [
      "DarkRed",
      "BlueViolet",
      "CadetBlue",
      "magenta",
      "blue",
      "lightblue",
    ];
    this.count = 0;
    this.index = 1;
    this.r = 0;
    this.g = 0;
    this.b = 0;
    // this.text = document.getElementById("id1").textContent.trim();
    this.text = "Hello, I'm Abheshekh Akula!";
    this.type();
  }
  type() {
    // console.log('typing');
    let ts = 80;
    if (this.count === 5) {
      this.count = 0;
    }
    if (this.index === this.text.length + 1) {
      this.index = 0;
      this.count += 1;
    }
    if (this.index === this.text.length) {
      ts = 1000;
    }
    if (this.text[this.index] !== " ") {
      if (this.r >= 256) this.r = 0;
      if (this.g >= 256) this.g = 0;
      if (this.b >= 256) this.b = 0;

      this.spanElemet.innerHTML = this.text.substring(0, this.index);

      //   console.log("TXT");
      //   console.log(this.spanElemet.textContent);
      //   this.spanElemet.style.color = this.colors[this.count];
      let colorStr = "rgba(" + this.r + "," + this.g + "," + this.b + ",1)";
      this.spanElemet.style.color = colorStr;
      //   console.log(this.index);
      //   console.log(this.index % 3 === 0);
      if (this.index % 3 === 0) this.r += 32;
      if (this.index % 3 === 1) this.g += 32;
      if (this.index % 3 === 2) this.r += 32;
      //   this.txtElemet.textContent = this.text.substring(this.index);
    }
    this.index += 1;
    setTimeout(() => this.type(), ts);
  }
}
document.addEventListener("DOMContentLoaded", init);
function init() {
  new typing();
}
